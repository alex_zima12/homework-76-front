import React from 'react';

const NewMessage = (props) => {
    return (
        <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert" id={props.id}>
            <div className="toast-header">
                <strong className="mr-auto">${props.author}</strong>
                <small className="text-muted">${props.date}</small>
            </div>
            <div className="toast-body">
                ${props.message}
            </div>
        </div>
    );
};

export default NewMessage;