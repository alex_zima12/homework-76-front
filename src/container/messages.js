import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages, fetchNewMessage, postMessageReq} from "../store/actions";
import NewMessage from "../components/NewMessage";


const Messages = () => {
    const [state, setState] = useState({
        author: "",
        message: ""
    });

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const error = useSelector(state => state.error);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);


    useEffect(() => {
        const interval = setInterval(() => {
           dispatch(fetchNewMessage(messages))
        }, 2000)
        return () => clearInterval(interval);
    }, [dispatch, messages]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const onClickHandler = () => {
        const obj = {
            message: state.message,
            author: state.author
        }
        console.log(obj, 'this');
        dispatch(postMessageReq(obj))
    };

    let errorMessage = '';
    if (error) {
        errorMessage = <h1>{error}</h1>
        return errorMessage;
    }

 let mesArray = [];
    messages.map(mes => {
        let newMes = <NewMessage
            key={mes.id}
            id={mes.id}
            author={mes.author}
            date={mes.date}
            message={mes.message}/>;
        return mesArray.push(newMes)
    });

    return (
    <div className="container bg-dark">
        {errorMessage}
        {mesArray}
        <form className="p-5">
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">@</span>
                </div>
                <input type="text" className="form-control" id="author" placeholder="Username" aria-label="Username"
                       aria-describedby="basic-addon1" onChange={inputChangeHandler} name="author"/>
            </div>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">With textarea</span>
                </div>
                <textarea className="form-control" aria-label="With textarea" id="message"
                          placeholder="Message" onChange={inputChangeHandler} name="message"/>
            </div>
            <button type="button" className="btn btn-info" id="send-btn" onClick={onClickHandler}>Send message</button>
        </form>
    </div>
    );
};

export default Messages;