import React from 'react';
import Messages from "./container/messages";

const App = () => {
    return (
        <div>
          <Messages/>
        </div>
    );
};

export default App;