import {FETCH_LAST_MESSAGE, FETCH_LAST_MESSAGE_ERROR, FETCH_MESSAGE_SUCCESS, POST_MESSAGE_ERROR} from "./actionTypes";

const initialState = {
    messages: [],
    error: null
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS:
            return {...state, messages: action.messages};
        case FETCH_LAST_MESSAGE:
            return {...state, messages: action.message};
        case POST_MESSAGE_ERROR:
            return {...state, error: action.error};
        case FETCH_LAST_MESSAGE_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default reducer;