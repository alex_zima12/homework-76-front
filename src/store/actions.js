import {FETCH_LAST_MESSAGE, FETCH_LAST_MESSAGE_ERROR, FETCH_MESSAGE_SUCCESS, POST_MESSAGE_ERROR} from "./actionTypes";
import axios from "../axiosApi";

const fetchMessageSuccess = (messages) => {
    return {type: FETCH_MESSAGE_SUCCESS, messages}
};

const fetchLastMesERror = error => {
    return {type: FETCH_LAST_MESSAGE_ERROR, error}
}

const fetchLastMessage = (message) => {
    return {type: FETCH_LAST_MESSAGE, message}
};

const postMessageError = error => {
    return {type: POST_MESSAGE_ERROR, error}
};

export const postMessageReq = message => {
    console.log(message);
    return async dispatch => {
        try {
            return await axios.post("/messages", message)
        } catch (e) {
            console.log(e);
            dispatch(postMessageError(e.response.data.error))
        }
    };
};

export const fetchNewMessage = (messages) => {
    return async dispatch => {
        try {
            const lastIndex = [messages.length - 1];
            const lastDate = messages[lastIndex];
            return await axios.get("/messages?datetime=" + lastDate.date).then(response => {
                if(response.data.length > 0){
                    dispatch(fetchLastMessage([
                        ...messages,
                        ...response.data
                    ]));
                }
            });
        } catch (e) {
            dispatch(fetchLastMesERror(e.response.data.error))
        }
    };
};

export const fetchMessages = () => {
    return dispatch => {
        return axios.get("/messages").then(response => {
            console.log(response);
            dispatch(fetchMessageSuccess(response.data));
        });
    };
};